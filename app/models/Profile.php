<?php
/**
 * User: Gabriel Acosta
 * Date: 12/18/13
 * Time: 8:39 AM
 */

class Profile extends Eloquent {
    public function user()
    {
        return $this->belongsTo('User');
    }
}