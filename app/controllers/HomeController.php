<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

    public function inicio($entidad){
//        $obj = new User();
//        $obj->username = "gabo";
//        $obj->email = "a@a.com";
//        $obj->save();

//        $user = new User();
//        $user->username = "Nuevo User";
//        $user->email = "b@a.com";
//
//        $user->save();
//
//        $profile = new Profile();
//        $profile->photo = 'foto.png';
//
//        $user->profile()->save($profile);


        $user = User::find(1);

//        $profile = new Profile();
//        $profile->photo = 'foto.png';
//
//        $user->profile()->save($profile);

//        var_dump($user->profile->photo);




        return View::make('eloquent',array('obj'=>$user , 'entidad'=>$entidad));

    }

}