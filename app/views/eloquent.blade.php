<?php
/**
 * User: Gabriel Acosta
 * Date: 12/18/13
 * Time: 8:24 AM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<h1>Bienvenido a la clase de Eloquent!</h1>
La entidad solicitada fue: {{ $entidad }}

<h3>Usuario {{ $obj->username }}</h3>

<h3>Email: {{ $obj->email }}</h3>

<h1>Foto: {{ $obj->profile->photo }}</h1>
</body>
</html>